#include "pc_detection/detector_node.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>



namespace pc_detection {

	DetectorNode::DetectorNode(const ros::NodeHandle &nh,
		const ros::NodeHandle &pnh)
	: nh_(nh),
	sub_pc_(nh_.subscribe("/icp_aligned_cloud", 1, &DetectorNode::PointCloudCb, this)),  //point_n is voxel grid /rgbd/depth/points_n
	pub_pc_floor_(nh_.advertise<sensor_msgs::PointCloud2>("floor", 1, false)),	//latch true, pub only 1 processed pointcloud
	pub_pc_wall1_(nh_.advertise<sensor_msgs::PointCloud2>("wall_1", 1, false)),
	pub_pc_wall2_(nh_.advertise<sensor_msgs::PointCloud2>("wall_2", 1, false)),
	pub_pc_obstacle_(nh_.advertise<sensor_msgs::PointCloud2>("obstacle", 1, false)),
	pub_intersection_point_marker_(nh_.advertise<visualization_msgs::Marker>("intersection_point", 1, false)),
	pub_poi_(nh_.advertise<visualization_msgs::Marker>("poi", 1, false)),

	pub_inliers_(nh_.advertise<sensor_msgs::PointCloud2>("other_except_floor", 1, false)),
	pub_pc_cornerframe_(nh_.advertise<sensor_msgs::PointCloud2>("pc_corner_frame", 1, false)),
	pub_plane_cluster_(nh_.advertise<sensor_msgs::PointCloud2>("plane_cluster", 1, false)),

	tfListener(tf_),
	tf_broadcaster_()
	{}




	// //https://pcl.readthedocs.io/en/latest/progressive_morphological_filtering.html?highlight=progressive_morphological_filtering
	void DetectorNode::groundFiltering(const PointCloudType::Ptr cloud){


	  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	  pcl::PointIndicesPtr ground (new pcl::PointIndices);


		// Create the filtering object
		pcl::ProgressiveMorphologicalFilter<pcl::PointXYZ> pmf;
		pmf.setInputCloud (cloud);
		pmf.setMaxWindowSize (20);
		pmf.setSlope (1.0f);
		pmf.setInitialDistance (0.1);
		pmf.setMaxDistance (0.2);
		pmf.extract (ground->indices);

		// Create the filtering object
		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud (cloud);
		extract.setIndices (ground);
		extract.filter (*cloud_filtered);

		std::cerr << "Ground cloud after filtering: " << std::endl;
		std::cerr << *cloud_filtered << std::endl;

		//pcl::PCDWriter writer;
		//writer.write<pcl::PointXYZ> ("/home/d/Desktop/samp11-utm_ground.pcd", *cloud_filtered, false);
        sensor_msgs::PointCloud2 ground_plane;
		pcl::toROSMsg(*cloud_filtered, ground_plane);
		pub_pc_floor_.publish(ground_plane);


		// Extract non-ground returns
		extract.setNegative (true);
		extract.filter (*cloud_filtered);
        sensor_msgs::PointCloud2 non_ground_plane;
		pcl::toROSMsg(*cloud_filtered, non_ground_plane);
		pub_inliers_.publish(non_ground_plane);

		std::cerr << "Object cloud after filtering: " << std::endl;
		std::cerr << *cloud_filtered << std::endl;

		//writer.write<pcl::PointXYZ> ("/home/d/Desktop/samp11-utm_object.pcd", *cloud_filtered, false);


		return;

	}



	void DetectorNode::mincutSegmentation (const PointCloudType::Ptr cloud){

		pcl::IndicesPtr indices (new std::vector <int>);
		pcl::removeNaNFromPointCloud(*cloud, *indices);

		pcl::MinCutSegmentation<pcl::PointXYZ> seg;
		seg.setInputCloud (cloud);
		seg.setIndices (indices);

		PointCloudType::Ptr foreground_points = PointCloudType::Ptr(new PointCloudType());
		pcl::PointXYZ point;
		point.x = 68.97;
		point.y = -18.55;
		point.z = 0.57;
		foreground_points->points.push_back(point);
		seg.setForegroundPoints (foreground_points);

		seg.setSigma (0.25);
		seg.setRadius (3.0433856);
		seg.setNumberOfNeighbours (14);
		seg.setSourceWeight (0.8);

		std::vector <pcl::PointIndices> clusters;
		seg.extract (clusters);

		std::cout << "Maximum flow is " << seg.getMaxFlow () << std::endl;

		pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = seg.getColoredCloud ();
		pcl::visualization::CloudViewer viewer ("Cluster viewer");
		viewer.showCloud(colored_cloud);

		while (!viewer.wasStopped ())
			{
			}


        return;
	}

	//https://github.com/PointCloudLibrary/pcl/blob/master/doc/tutorials/content/sources/interactive_icp/interactive_icp.cpp
	void DetectorNode::getICP(){

		std::vector<path> pc_paths = getPointcloudFiles();
		int iterations = 50;
		PointCloudType::Ptr global_cloud = PointCloudType::Ptr(new PointCloudType());

		PointCloudType::Ptr target = PointCloudType::Ptr(new PointCloudType());
		pcl::io::loadPCDFile<pcl::PointXYZ> (pc_paths[0].string(), *target);



		for (int idx = 1; idx < pc_paths.size(); idx++){


			PointCloudType::Ptr source = PointCloudType::Ptr(new PointCloudType());

			pcl::io::loadPCDFile<pcl::PointXYZ> (pc_paths[idx].string(), *source);


			pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
			icp.setMaximumIterations (iterations);

			icp.setMaxCorrespondenceDistance (0.05);
//			icp.setTransformationEpsilon (1e-8);
//			icp.setEuclideanFitnessEpsilon (0.01);
//			icp.setRANSACOutlierRejectionThreshold(0.05);

			icp.setInputSource (source);
			icp.setInputTarget (target);
			icp.align (*source);

			*target += *source;

			PointCloudType::Ptr target_voxel = PointCloudType::Ptr(new PointCloudType());

			// Create the filtering object
			pcl::VoxelGrid<pcl::PointXYZ> sor;
			sor.setLeafSize (0.02f, 0.02f, 0.02f);
			sor.setInputCloud (target);
			sor.filter (*target_voxel);

			*target = *target_voxel;

		}



		PointCloudType::Ptr final_v = PointCloudType::Ptr(new PointCloudType());
		// Create the filtering object
		pcl::VoxelGrid<pcl::PointXYZ> sor;
		sor.setLeafSize (0.07f, 0.07f, 0.07f);
		sor.setInputCloud (target);
		sor.filter (*final_v);
		writer.write<pcl::PointXYZ> ("/home/d/Desktop/global_cloud_1_voxel_new.pcd", *final_v, false);
		std::cout << "file written-------------------" << std::endl ;

	}


	std::vector<path>  DetectorNode::getPointcloudFiles() {


		std::vector<int> file_nums;
		std::vector<path> file_paths;

	    path p ("/media/d/SSD_T5/06.24/obstacle/pointclouds_for_icp_0624_1/");


		 directory_iterator end_itr;
		// cycle through the directory
		for (directory_iterator itr(p); itr != end_itr; ++itr)
		{
			// If it's not a directory, list it. If you want to list directories too, just remove this check.
			if (is_regular_file(itr->path())) {
				// assign current file name to current_file and echo it out to the console.
				file_paths.push_back(itr->path());

				std::string current_file_name = itr->path().filename().string();
				current_file_name = current_file_name.substr(3, current_file_name.size() - 1);
				file_nums.push_back(std::stoi(current_file_name));
			}
		}


		std::vector<size_t> file_idxs(file_nums.size());
		std::iota(file_idxs.begin(), file_idxs.end(), 0);

		// sort indexes based on comparing values in v
		// using std::stable_sort instead of std::sort
		// to avoid unnecessary index re-orderings
		// when v contains elements of equal values
		std::stable_sort(file_idxs.begin(), file_idxs.end(),
		   [&file_nums](size_t i1, size_t i2) {return file_nums[i1] < file_nums[i2];});

		// sort dists
		std::sort(file_nums.begin(), file_nums.end());


//		std::cout << "Sorted \n";
//		for (auto x : file_nums)
//			std::cout << x << " ";
//		std::cout << std::endl;
//		for (auto x : file_idxs)
//			std::cout << x << " ";
//		std::cout << std::endl;


		std::vector<path> file_paths_sorted;
	    for (int idx = 0; idx < file_idxs.size(); idx++) {
//			std::cout << file_paths[file_idxs[idx]] << std::endl;
			file_paths_sorted.push_back(file_paths[file_idxs[idx]]);
		}

//		for (auto x : file_paths_sorted)
//			std::cout << x << std::endl << std::endl;


//		std::cout << file_paths[file_idxs[0]] << std::endl;
//		std::cout << file_paths[file_idxs[1]] << std::endl;
//		std::cout << file_paths[file_idxs[2]] << std::endl;


		return file_paths_sorted;
	}




    void DetectorNode::clusterExtraction(const PointCloudType::Ptr cloud){


    	// Creating the KdTree object for the search method of the extraction
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
		tree->setInputCloud (cloud);

		std::vector<pcl::PointIndices> cluster_indices;
		pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
		ec.setClusterTolerance (0.06); // 2cm
		ec.setMinClusterSize (100);
		ec.setMaxClusterSize (25000);
		ec.setSearchMethod (tree);
		ec.setInputCloud (cloud);
		ec.extract (cluster_indices);


		std::cout << "Size of cluster_indices: " << cluster_indices.size() << std::endl;
		int j = 0;
		pcl::PointCloud<pcl::PointXYZ>::Ptr correct_cluster (new pcl::PointCloud<pcl::PointXYZ>);
		//pcl::PointCloud<pcl::PointXYZ>::Ptr copy_cluster (new pcl::PointCloud<pcl::PointXYZ>);

		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
		{
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
			for (const auto& idx : it->indices)
			  cloud_cluster->push_back ((*cloud)[idx]); //*
			cloud_cluster->width = cloud_cluster->size ();
			cloud_cluster->height = 1;
			cloud_cluster->is_dense = true;
			//*copy_cluster= *cloud_cluster;

			std::cout << "PointCloud representing the Cluster: " << cloud_cluster->size () << " data points." << std::endl;
			if (checkPlanes(cloud_cluster)){
				*correct_cluster = *cloud_cluster; //copy_cluster
				//break;
			}
			j++;
		}

        sensor_msgs::PointCloud2 cluster_msg;
		pcl::toROSMsg(*correct_cluster, cluster_msg);
		cluster_msg.header.frame_id = "base_link";
		cluster_msg.header.stamp = ros::Time().now();
		pub_plane_cluster_.publish(cluster_msg);
    	return;
    }


    bool DetectorNode::checkPlanes(const PointCloudType::Ptr cloud_cluster){


		pcl::SACSegmentation<pcl::PointXYZ> seg;
		// Optional
		seg.setOptimizeCoefficients(true);
		Eigen::Vector3f axis_z = Eigen::Vector3f(0.0,0.0,1.0);
		seg.setAxis(axis_z);
		seg.setEpsAngle(5.0f * (M_PI/180.0f));

		// Mandatory
		seg.setModelType(pcl::SACMODEL_PARALLEL_PLANE);
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setDistanceThreshold (0.1);

		int _min_percentage = 90;
		int original_size(cloud_cluster->height*cloud_cluster->width);
		int n_planes(0);


		pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
		pcl::ModelCoefficients::Ptr coefficients = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);

		while (cloud_cluster->height*cloud_cluster->width>original_size*_min_percentage/100)
		{
			// Segment the largest planar component from the remaining cloud
			seg.setInputCloud (cloud_cluster);
			seg.segment (*inliers, *coefficients);
			if (inliers->indices.size () == 0)
			{
			  std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
			  return false;
			}

			// Extract the planar inliers from the input cloud
			pcl::ExtractIndices<pcl::PointXYZ> extract;
			extract.setInputCloud (cloud_cluster);
			extract.setIndices (inliers);
			extract.setNegative (false);

			// Get the points associated with the planar surface
			pcl::PointCloud<pcl::PointXYZ> temp_cloud_segmented;
			extract.filter (temp_cloud_segmented);
			std::cout << "PointCloud representing the temp_cloud_segmented component: " << temp_cloud_segmented.points.size () << " data points." << std::endl;

			// Remove the planar inliers, extract the rest
			PointCloudType::Ptr cloud_rest = PointCloudType::Ptr(new PointCloudType());
			extract.setNegative (true);
			extract.filter (*cloud_rest);
			cloud_cluster->swap(*cloud_rest);


			// Display infor
			ROS_INFO("%s: fitted plane %i: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
					 _name.c_str(),n_planes,
					 coefficients->values[0],(coefficients->values[1]>=0?"+":""),
					 coefficients->values[1],(coefficients->values[2]>=0?"+":""),
					 coefficients->values[2],(coefficients->values[3]>=0?"+":""),
					 coefficients->values[3],
					 inliers->indices.size(),original_size);
			ROS_INFO("%s: points left in cluster cloud %i",_name.c_str(),cloud_cluster->width*cloud_cluster->height);

			// Nest iteration
			n_planes++;
			std::cout << "n_planes in cluster: --> : " << n_planes << std::endl;

		}

		return true;
        }


	DetectorNode::PointCloudType::Ptr DetectorNode::setPassthroughFilter(const PointCloudType::Ptr pc_in_baselink){

	    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;

		// Filtered cloud
	    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	    pcl::PointCloud<pcl::PointXYZ>::Ptr non_floor_cloud (new pcl::PointCloud<pcl::PointXYZ>);

		pcl::PassThrough<pcl::PointXYZ> pass;
		pass.setInputCloud (pc_in_baselink);
		pass.setFilterFieldName ("z");
		pass.setFilterLimits (0.0, 0.2);
		pass.setFilterLimitsNegative (false);
		pass.filter (*cloud_filtered);
		pass.setFilterLimitsNegative (true);
		pass.filter (*non_floor_cloud);

		pcl::PointIndices::Ptr inliers_floor (new pcl::PointIndices);
		pcl::PointIndices::Ptr inliers_all_except_floor (new pcl::PointIndices);

		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg_floor;
		// Optional
		seg_floor.setOptimizeCoefficients(true);
		seg_floor.setModelType(pcl::SACMODEL_PLANE);
		seg_floor.setMethodType(pcl::SAC_RANSAC);
		seg_floor.setDistanceThreshold (0.1);

		seg_floor.setInputCloud(cloud_filtered);
		seg_floor.segment(*inliers_floor, *coefficients_floor);

		if (inliers_floor->indices.size() == 0)
		{
			PCL_ERROR("Could not estimate a planar model for the given dataset.\n");
			//return;
		}

		plane_floor << coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2], coefficients_floor->values[3];

		cloud_segmented_floor = *cloud_filtered;
	    std::cout << "PointCloud representing the cloud_segmented_floor component: " << cloud_segmented_floor.points.size () << " data points." << std::endl;
	    std::cout << "PointCloud representing the all_except_floor (segm) component: " << non_floor_cloud->points.size () << " data points." << std::endl;

	    sensor_msgs::PointCloud2 segm_no_floor;
        pcl::toROSMsg(*non_floor_cloud, segm_no_floor);
        pub_inliers_.publish(segm_no_floor);

	    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;

	    ROS_INFO("%s: fitted plane floor: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
	    					 _name.c_str(),
							 coefficients_floor->values[0],(coefficients_floor->values[1]>=0?"+":""),
							 coefficients_floor->values[1],(coefficients_floor->values[2]>=0?"+":""),
							 coefficients_floor->values[2],(coefficients_floor->values[3]>=0?"+":""),
							 coefficients_floor->values[3],
							 inliers_floor->indices.size(), pc_in_baselink->height*pc_in_baselink->width);

//		writer.write<pcl::PointXYZ> ("/home/d/Desktop/global_cloud_2_nonfloor.pcd", *segm, false);

	    return non_floor_cloud;

	}


	DetectorNode::PointCloudType::Ptr DetectorNode::getFloorPlanePerpendicularZAxis(const PointCloudType::Ptr pc_in_baselink){

		std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;
		PointCloudType::Ptr segm = PointCloudType::Ptr(new PointCloudType());

		// Floor
		// do passthrough once before looking for perpendicular plane



		pcl::PointIndices::Ptr inliers_floor (new pcl::PointIndices);
		pcl::PointIndices::Ptr inliers_all_except_floor (new pcl::PointIndices);

		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg_floor;
		// Optional
		seg_floor.setOptimizeCoefficients(true);
		Eigen::Vector3f axis = Eigen::Vector3f(0.0,0.0,1.0);
		seg_floor.setAxis(axis);
		seg_floor.setEpsAngle(30.0f * (M_PI/180.0f));

		// Mandatory
		seg_floor.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
		seg_floor.setMethodType(pcl::SAC_RANSAC);
		seg_floor.setDistanceThreshold (0.1);

		seg_floor.setInputCloud(pc_in_baselink);
		seg_floor.segment(*inliers_floor, *coefficients_floor);

		if (inliers_floor->indices.size() == 0)
		{
			PCL_ERROR("Could not estimate a planar model for the given dataset.\n");
			//return;
		}

		plane_floor << coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2], coefficients_floor->values[3];

		// Create the filtering object
		pcl::ExtractIndices<pcl::PointXYZ> extract_floor;
		extract_floor.setInputCloud(pc_in_baselink);
		extract_floor.setIndices(inliers_floor);
		extract_floor.setNegative(false);
		extract_floor.filter(cloud_segmented_floor);

		std::cout << "PointCloud representing the cloud_segmented_floor component: " << cloud_segmented_floor.points.size () << " data points." << std::endl;

		// Remove the planar inliers, extract the rest
		extract_floor.setNegative (true);
		extract_floor.filter (*segm);



		std::cout << "PointCloud representing the all_except_floor (segm) component: " << segm->points.size () << " data points." << std::endl;
		sensor_msgs::PointCloud2 segm_no_floor;
		pcl::toROSMsg(*segm, segm_no_floor);
		pub_inliers_.publish(segm_no_floor);

		std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;

		ROS_INFO("%s: fitted plane floor: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
							 _name.c_str(),
							 coefficients_floor->values[0],(coefficients_floor->values[1]>=0?"+":""),
							 coefficients_floor->values[1],(coefficients_floor->values[2]>=0?"+":""),
							 coefficients_floor->values[2],(coefficients_floor->values[3]>=0?"+":""),
							 coefficients_floor->values[3],
							 inliers_floor->indices.size(), pc_in_baselink->height*pc_in_baselink->width);

	//		writer.write<pcl::PointXYZ> ("/home/d/Desktop/global_cloud_2_nonfloor.pcd", *segm, false);

		return segm;
		}


	void DetectorNode::getWalls(const PointCloudType::Ptr segment_except_floor){


		// 2 walls search

		// Create the segmentation object
		pcl::SACSegmentation<pcl::PointXYZ> seg;
		// Optional
		seg.setOptimizeCoefficients(true);
		Eigen::Vector3f axis_z = Eigen::Vector3f(0.0,0.0,1.0);
		seg.setAxis(axis_z);
		seg.setEpsAngle(5.0f * (M_PI/180.0f)); // first is in degree, ie 2,10 deg. less than 5.0 cannot find inliers exception


		// Mandatory
		seg.setModelType(pcl::SACMODEL_PARALLEL_PLANE  );
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setDistanceThreshold (0.02);

		int _min_percentage = 40;
		int original_size(segment_except_floor->height*segment_except_floor->width);
		int n_planes(0);

		std::vector<pcl::ModelCoefficients> plane_coeff_list_except_floorplane;
		std::vector<pcl::PointCloud<pcl::PointXYZ>> planes_list_except_floorplane;
		std::vector<pcl::PointIndices> plane_indices;

		pcl::PointIndices::Ptr inliers = pcl::PointIndices::Ptr(new pcl::PointIndices);
        pcl::ModelCoefficients::Ptr coefficients = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);

		while (segment_except_floor->height*segment_except_floor->width>original_size*_min_percentage/100)
		{

			// Segment the largest planar component from the remaining cloud
			seg.setInputCloud (segment_except_floor);
			seg.segment (*inliers, *coefficients);
			if (inliers->indices.size () == 0)
			{
			  std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
			  break;
			}

			// Extract the planar inliers from the input cloud
			pcl::ExtractIndices<pcl::PointXYZ> extract;
			extract.setInputCloud (segment_except_floor);
			extract.setIndices (inliers);
			extract.setNegative (false);
			plane_coeff_list_except_floorplane.push_back(*coefficients);
			plane_indices.push_back(*inliers);


			// Get the points associated with the planar surface
			pcl::PointCloud<pcl::PointXYZ> temp_cloud_segmented;
			extract.filter (temp_cloud_segmented);
			std::cout << "PointCloud representing the temp_cloud_segmented component: " << temp_cloud_segmented.points.size () << " data points." << std::endl;
			planes_list_except_floorplane.push_back(temp_cloud_segmented);

			// Remove the planar inliers, extract the rest
			extract.setNegative (true);
			extract.filter (*cloud_f);
			segment_except_floor->swap(*cloud_f);


			// Display infor
			ROS_INFO("%s: fitted plane %i: %fx%s%fy%s%fz%s%f=0 (inliers: %zu/%i)",
					 _name.c_str(),n_planes,
					 coefficients->values[0],(coefficients->values[1]>=0?"+":""),
					 coefficients->values[1],(coefficients->values[2]>=0?"+":""),
					 coefficients->values[2],(coefficients->values[3]>=0?"+":""),
					 coefficients->values[3],
					 inliers->indices.size(),original_size);
			ROS_INFO("%s: points left in cloud %i",_name.c_str(),segment_except_floor->width*segment_except_floor->height);

			// Nest iteration
			n_planes++;
			std::cout << "n_planes: --> : " << n_planes << std::endl;

		}

		std::cout << "plane_coeff_list_except_floorplane.size(): " << plane_coeff_list_except_floorplane.size() << std::endl;

	//		writer.write<pcl::PointXYZ> ("/home/d/Desktop/global_cloud_2_nonplanar.pcd", *segm, false);


//	    pcl::PointXYZ poi_point(0.5, 0.6, 0.0); //in corner frame
//	    pcl::PointXYZ poi_point(2.0, 0.25, 0.0); //need to give in baselink //outer_corner_merged_pointcloud_obstacle
//	    pcl::PointXYZ poi_point(1.8, -0.05, 0.0); //merged_pointcloud_obstacle


//	    //global_cloud_1
//	    pcl::PointXYZ poi_point(2.3, 0.0, 0.0);
//
//	    //global_cloud_2
		pcl::PointXYZ poi_point(2.20, 0.02, 0.03);



		visualization_msgs::Marker marker_poi;
		marker_poi.header.frame_id = "base_link";
		marker_poi.header.stamp = ros::Time();
		marker_poi.type = visualization_msgs::Marker::POINTS;
		marker_poi.action = visualization_msgs::Marker::ADD;
		marker_poi.scale.x = 0.1;
		marker_poi.scale.y = 0.1;
		marker_poi.scale.z = 0.1;
		marker_poi.color.a = 1.0;
		marker_poi.color.r = 1.0;
		marker_poi.color.g = 0.0;
		marker_poi.color.b = 0.0;


		geometry_msgs::Point p_poi;

		p_poi.x = poi_point.x;
		p_poi.y = poi_point.y;
		p_poi.z = poi_point.z;

		std_msgs::ColorRGBA c_poi;
		c_poi.r = 1.0;
		c_poi.g = 0.0;
		c_poi.b = 0.0;
		c_poi.a = 1.0;

		marker_poi.points.push_back(p_poi);
		marker_poi.colors.push_back(c_poi);

		pub_poi_.publish(marker_poi);


		std::vector<double> dists;

		for (int idx = 0; idx < plane_coeff_list_except_floorplane.size(); idx++) {
			std::cout << idx << std::endl;
			pcl::ModelCoefficients plane_coefficients = plane_coeff_list_except_floorplane[idx];
			std::cout << plane_coefficients.values[0] << " , " << plane_coefficients.values[1] << " , " << plane_coefficients.values[2] << " , " << plane_coefficients.values[3] << std::endl;

			Eigen::Vector4f ground_plane_params = Eigen::Vector4f(	plane_coefficients.values[0],
																	plane_coefficients.values[1],
																	plane_coefficients.values[2],
																	plane_coefficients.values[3]);

			double dist_wall = pcl::pointToPlaneDistance(poi_point, ground_plane_params);
			std::cout << "dist_wall: " << dist_wall << std::endl;
			dists.push_back(dist_wall);
		}


		//https://stackoverflow.com/questions/10580982/c-sort-keeping-track-of-indices
		//codegrepper.com/code-examples/cpp/c%2B%2B+sorting+and+keeping+track+of+indexes
		std::vector<size_t> indices(dists.size());
		std::iota(indices.begin(), indices.end(), 0);
		std::stable_sort(indices.begin(), indices.end(),
		   [&dists](size_t i1, size_t i2) {return dists[i1] < dists[i2];});

		// sort dists
		std::sort(dists.begin(), dists.end());


		std::cout << "Sorted \n";
		for (auto x : dists)
			std::cout << x << " ";
		std::cout << std::endl;
		std::cout << "minElement1:" << dists[0] << std::endl;
		std::cout << "minElement2:" << dists[1] << std::endl;

		for (auto x : indices)
			std::cout << x << " ";
		std::cout << std::endl;


		*coefficients_wall1 = plane_coeff_list_except_floorplane[indices[0]];
		*coefficients_wall2 = plane_coeff_list_except_floorplane[indices[1]];

		plane_wall1 << coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2], coefficients_wall1->values[3];
		plane_wall2 << coefficients_wall2->values[0], coefficients_wall2->values[1], coefficients_wall2->values[2], coefficients_wall2->values[3];

		cloud_segmented_wall1 = planes_list_except_floorplane[indices[0]];
		cloud_segmented_wall2 = planes_list_except_floorplane[indices[1]];

		*inliers_wall1 = plane_indices[indices[0]];
		*inliers_wall2 = plane_indices[indices[1]];
		return ;
	}


	DetectorNode::PointCloudType::Ptr  DetectorNode::removePlanars(const PointCloudType::Ptr cloud) {

		PointCloudType::Ptr non_planar_cloud = PointCloudType::Ptr(new PointCloudType());
		PointCloudType::Ptr non_planar_cloud2 = PointCloudType::Ptr(new PointCloudType());



		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud(cloud);
		extract.setIndices(inliers_wall1);
		extract.setNegative(true);
		extract.filter(*non_planar_cloud);

		extract.setInputCloud(non_planar_cloud);
		extract.setIndices(inliers_wall2);
		extract.setNegative(true);
		extract.filter(*non_planar_cloud2);

		return non_planar_cloud2;

	}

	void DetectorNode::getIntersectionPoint(const PointCloudType::Ptr pc_in_baselink) {



		// Planar segmentation for each plane wall1, wall2, floor
	    std::cout << "PointCloud representing the pc_in_baselink component: " << pc_in_baselink->points.size () << " data points." << std::endl;


	    //Floor
	    segm = getFloorPlanePerpendicularZAxis(pc_in_baselink);
		//PointCloudType::Ptr floor_removed_cloud = PointCloudType::Ptr(new PointCloudType());
		//*floor_removed_cloud = *segm;


		// 2 walls search
	    getWalls(segm);
	    //no_planars_cloud = removePlanars(floor_removed_cloud);

		bool ans = pcl::threePlanesIntersection (plane_wall1, plane_wall2, plane_floor, intersection_point, 1e-3);
		std::cout << "intersecting --> : " << std::noboolalpha << ans << std::endl;
		// intersection_point is translation part of transform matrix (corner coord. to baselink)
		std::cout << "point --> : " << intersection_point << std::endl;



		visualization_msgs::Marker marker;
		marker.header.frame_id = "base_link";
		marker.header.stamp = ros::Time();
		marker.type = visualization_msgs::Marker::POINTS;
		marker.action = visualization_msgs::Marker::ADD;
		marker.scale.x = 0.1;
		marker.scale.y = 0.1;
		marker.scale.z = 0.1;
		marker.color.a = 1.0;
		marker.color.r = 0.0;
		marker.color.g = 1.0;
		marker.color.b = 0.0;

		geometry_msgs::Point p;

		p.x = intersection_point[0];
		p.y = intersection_point[1];
		p.z = intersection_point[2];

		std_msgs::ColorRGBA c;
		c.r = 0.0;
		c.g = 0.0;
		c.b = 1.0;
		c.a = 1.0;

		marker.points.push_back(p);
		marker.colors.push_back(c);


		pub_intersection_point_marker_.publish(marker);



        //Publish the new cloud
        sensor_msgs::PointCloud2 segmented_output_floor;
		pcl::toROSMsg(cloud_segmented_floor, segmented_output_floor);
		pub_pc_floor_.publish(segmented_output_floor);


        sensor_msgs::PointCloud2 segmented_output_wall1;
        pcl::toROSMsg(cloud_segmented_wall1, segmented_output_wall1);
        pub_pc_wall1_.publish(segmented_output_wall1);


        sensor_msgs::PointCloud2 segmented_output_wall2;
        pcl::toROSMsg(cloud_segmented_wall2, segmented_output_wall2);
        pub_pc_wall2_.publish(segmented_output_wall2);


	}




	DetectorNode::PointCloudType::Ptr DetectorNode::getTransformedObstacleCloud (const Eigen::Vector3d intersection_point,
													const PointCloudType::Ptr obstacle_in_baselink) {



	    Eigen::Vector3d floor(coefficients_floor->values[0], coefficients_floor->values[1], coefficients_floor->values[2]);
	    Eigen::Vector3d wall1(coefficients_wall1->values[0], coefficients_wall1->values[1], coefficients_wall1->values[2]);
	    Eigen::Vector3d wall2(coefficients_wall2->values[0], coefficients_wall2->values[1], coefficients_wall2->values[2]);


		//now, calculate rotation part of transform matrix
	    Eigen::Vector3d n_xy(floor[0], floor[1], floor[2]);

	    std::cout << "n_xy:\n" << n_xy << std::endl;


	    //Determine corner frame's coordinate system
	    //4 unit vectors centered at intersection point

	    Eigen::Vector3d n_yz(wall1[0], wall1[1], wall1[2]);
	    Eigen::Vector3d neg_n_yz(-wall1[0], -wall1[1], -wall1[2]);

	    Eigen::Vector3d n_xz = n_xy.cross(n_yz);
	    Eigen::Vector3d neg_n_xz(-n_xz[0], -n_xz[1], -n_xz[2]);


	    std::vector<Eigen::Vector3d> vectors;
	    vectors.push_back(n_yz);
	    vectors.push_back(neg_n_yz);
	    vectors.push_back(n_xz);
	    vectors.push_back(neg_n_xz);


	    std::cout << "n_yz :  " << n_yz << std::endl;
	    std::cout << "neg_n_yz :  " << neg_n_yz << std::endl;
	    std::cout << "n_xz :  " << n_xz << std::endl;
	    std::cout << "neg_n_xz :  " << neg_n_xz << std::endl;



	    Eigen::Vector3d baselink_dist(intersection_point[0], intersection_point[1], intersection_point[2]); //points inward corner neg.sign

	    double angle1 =  std::atan2(n_yz.cross(baselink_dist).norm(), n_yz.dot(baselink_dist));
	    double angle2 =  std::atan2(neg_n_yz.cross(baselink_dist).norm(), neg_n_yz.dot(baselink_dist));
	    double angle3 =  std::atan2(n_xz.cross(baselink_dist).norm(), n_xz.dot(baselink_dist));
	    double angle4 =  std::atan2(neg_n_xz.cross(baselink_dist).norm(), neg_n_xz.dot(baselink_dist));



	    std::vector<double> angles;
	    angles.push_back(angle1);
	    angles.push_back(angle2);
	    angles.push_back(angle3);
	    angles.push_back(angle4);


	    std::vector<size_t> v_idxs(angles.size());
		std::iota(v_idxs.begin(), v_idxs.end(), 0);
		std::stable_sort(v_idxs.begin(), v_idxs.end(),
		   [&angles](size_t i1, size_t i2) {return angles[i1] > angles[i2];});

		//2 main unit vectors x, y
		std::cout << "maxElement1:" << vectors[v_idxs[0]] << std::endl;
		std::cout << "maxElement2:" << vectors[v_idxs[1]] << std::endl;
		Eigen::Vector3d max1 = vectors[v_idxs[0]];
		Eigen::Vector3d max2 = vectors[v_idxs[1]];

		Eigen::Vector3d u_x;
		Eigen::Vector3d u_y;

		//Determine which is x, which is y
		//compare y-coord according to baselink
		std::cout << "max1[1]:" << max1[1] << std::endl;
		std::cout << "max2[1]:" << max2[1] << std::endl;

		if (max1[1] > 0){
			u_x = max1;
			u_y = max2;
		}
		else{
			u_x = max2;
			u_y = max1;
		}


		//print for angle debug
		for (int i=0;i<=angles.size();i++) {
			if (angles[i] * 180/M_PI > 90)
				angles[i] = - (180 - angles[i]* 180/M_PI) * M_PI/180;
		}

	    std::cout << "angle1 :  " << angles[0]* 180/M_PI << std::endl;
	    std::cout << "angle2 :  " << angles[1]* 180/M_PI << std::endl;
	    std::cout << "angle3 :  " << angles[2]* 180/M_PI << std::endl;
	    std::cout << "angle4 :  " << angles[3]* 180/M_PI << std::endl;



        Eigen::Matrix3d rotation_matrix;
        rotation_matrix << 	u_x[0], u_y[0], n_xy[0],
        					u_x[1], u_y[1], n_xy[1],
							u_x[2], u_y[2], n_xy[2];


	    Eigen::Quaterniond rot_q(rotation_matrix);
	    std::cout << "rot_q.x():\n" << rot_q.x() << std::endl;
	    std::cout << "rot_q.y():\n" << rot_q.y() << std::endl;
	    std::cout << "rot_q.z():\n" << rot_q.z() << std::endl;
	    std::cout << "rot_q.w():\n" << rot_q.w() << std::endl;

	    Eigen::Translation3d trans(intersection_point[0], intersection_point[1], intersection_point[2]);
 	    corner_to_baselink_tf  = Eigen::Isometry3d(trans * rot_q);



	    std::cout << "corner_to_baselink_tf translation:\n" << corner_to_baselink_tf.translation() << std::endl;
	    std::cout << "corner_to_baselink_tf rotation:\n" << corner_to_baselink_tf.rotation() << std::endl;


	    Eigen::Isometry3d corner_to_baselink_tf_ = corner_to_baselink_tf.inverse();
	    std::cout << "inverse translation:\n" << corner_to_baselink_tf_.translation() << std::endl;
		std::cout << "inverse rotation:\n" << corner_to_baselink_tf_.rotation() << std::endl;

		Eigen::Quaterniond q(corner_to_baselink_tf_.rotation());
	    std::cout << "q.x():\n" << q.x() << std::endl;
	    std::cout << "q.y():\n" << q.y() << std::endl;
	    std::cout << "q.z():\n" << q.z() << std::endl;
	    std::cout << "q.w():\n" << q.w() << std::endl;


		std::cout << "corner_to_baselink_tf_ rows:\n" << corner_to_baselink_tf_.rows() << std::endl;
		std::cout << "corner_to_baselink_tf_ cols:\n" << corner_to_baselink_tf_.cols() << std::endl;



		// Transform publish
		geometry_msgs::TransformStamped transformStamped;
		transformStamped.header.stamp = ros::Time::now();
		transformStamped.header.frame_id = "corner_frame"; 	// corner_frame baselink to corner frame corner_to_baselink_tf_ = corner_to_baselink_tf.inverse()
		transformStamped.child_frame_id = "base_link";		// base_link (inverse of (corner to baselink = corner_to_baselink_tf))

		transformStamped.transform.translation.x = corner_to_baselink_tf_.translation().x();
		transformStamped.transform.translation.y = corner_to_baselink_tf_.translation().y();
		transformStamped.transform.translation.z = corner_to_baselink_tf_.translation().z();

		transformStamped.transform.rotation.x = q.x();
		transformStamped.transform.rotation.y = q.y();
		transformStamped.transform.rotation.z = q.z();
		transformStamped.transform.rotation.w = q.w();
		tf_broadcaster_.sendTransform(transformStamped);
		//



		PointCloudType::Ptr obstacle_in_corner_frame = PointCloudType::Ptr(new PointCloudType());
	    for (int idx = 0; idx < obstacle_in_baselink->points.size(); idx++) {

	    	Eigen::Vector3d p_in_b(obstacle_in_baselink->points[idx].x, obstacle_in_baselink->points[idx].y, obstacle_in_baselink->points[idx].z);
	    	Eigen::Vector3d p_in_c = Eigen::Vector3d(corner_to_baselink_tf_ * p_in_b);



	    	pcl::PointXYZ p_in_c_cloud_point(p_in_c[0], p_in_c[1], p_in_c[2]);
	    	obstacle_in_corner_frame->push_back(p_in_c_cloud_point);
	    }


	    std::cout << "obstacle_in_baselink size: " << obstacle_in_baselink->points.size() << std::endl;
	    std::cout << "obstacle_in_corner_frame size: " << obstacle_in_corner_frame->points.size() << std::endl;


	    return obstacle_in_corner_frame;
	}


	void DetectorNode::getObstacleCropbox(const PointCloudType::Ptr pc_in_baselink) {

	    std::cout << "segm size: " << pc_in_baselink->points.size() << std::endl;


        PointCloudType::Ptr pc_tc = PointCloudType::Ptr(new PointCloudType());
        PointCloudType::Ptr pc_in_corner_frame = getTransformedObstacleCloud(intersection_point, pc_in_baselink);

	    std::cout << "segm size: " << pc_in_baselink->points.size() << std::endl;

		sensor_msgs::PointCloud2 pc_in_corner_frame_cloud;
		pcl::toROSMsg(*pc_in_corner_frame, pc_in_corner_frame_cloud);
		pc_in_corner_frame_cloud.header.frame_id = "corner_frame";
		pc_in_corner_frame_cloud.header.stamp = ros::Time::now();
		pub_pc_cornerframe_.publish(pc_in_corner_frame_cloud);

		double output_x_min = 0.2; //0.0;
		double output_x_max = 1.0;
		double output_y_min = 0.2;
		double output_y_max =  1.0;


		pcl::CropBox<pcl::PointXYZ> crop_box;
		crop_box.setMin(Eigen::Vector4f(output_x_min, output_y_min, 0.0, 1));
		crop_box.setMax(Eigen::Vector4f(output_x_max, output_y_max,  RANGE_MAX, 1));
		crop_box.setInputCloud(pc_in_corner_frame);
		crop_box.filter(*pc_tc);


		std::cout << "PointCloud representing the crop_box component: " << pc_tc->points.size () << " data points." << std::endl;

		sensor_msgs::PointCloud2 left_cloud;
		pcl::toROSMsg(*pc_tc, left_cloud);
		left_cloud.header.frame_id = "corner_frame";
		left_cloud.header.stamp = ros::Time::now();
		pub_pc_obstacle_.publish(left_cloud);



	}

	void DetectorNode::PointCloudCb(const sensor_msgs::PointCloud2ConstPtr &pc2_input_) {



		pc2_input__ = *pc2_input_;
		std::cout<< "pc2_input__.header.frame_id : "<< pc2_input__.header.frame_id << std::endl;
		std::cout<< "pc2_input__.header.stamp : "<< pc2_input__.header.stamp << std::endl << std::endl;
//		 pub_pc_.publish(pc2_input__);



		PointCloudType::Ptr pc_in = PointCloudType::Ptr(new PointCloudType());
		fromROSMsg(pc2_input__, *pc_in);


		// Transforming to base_link
		sensor_msgs::PointCloud2 output;
		toROSMsg(*pc_in, output);

		geometry_msgs::TransformStamped transformStamped;

		try{
			transformStamped = tf_.lookupTransform("base_link", "rgbd_depth_optical_frame", ros::Time(0),
												  ros::Duration(3.0));

			ROS_INFO("pc lookup transform rgbd_depth_optical_frame->base_link success");

		}
		catch (tf2::TransformException &ex) {
			ROS_WARN("Could NOT transform : %s", ex.what());
			ros::Duration(1.0).sleep();
		}

		tf::Transform transform;
		tf::transformMsgToTF(transformStamped.transform, transform);
		std::cout<< "transformStamped.header.frame_id : "<< transformStamped.header.frame_id << std::endl;
		std::cout<< "transformStamped.header.stamp : "<< transformStamped.header.stamp << std::endl;
		std::cout<< "transformStamped.child_frame_id : "<< transformStamped.child_frame_id << std::endl << std::endl;



		sensor_msgs::PointCloud2 out;
		std::string target_frame = "base_link";

		pcl_ros::transformPointCloud(target_frame, transform, output, out);
		std::cout<< "out.header.frame_id : "<< out.header.frame_id << std::endl;
		std::cout<< "out.header.stamp : "<< out.header.stamp << std::endl << std::endl;

		//pub_pc_.publish(out);

		// Transform done
		PointCloudType::Ptr pc_in_baselink = PointCloudType::Ptr(new PointCloudType());
		fromROSMsg(out, *pc_in_baselink);


//		groundFiltering(pc_in_baselink);
//		mincutSegmentation(pc_in_baselink);
		getIntersectionPoint(pc_in_baselink);
//		getObstacleCropbox(no_planars_cloud);   //cloud_f //pc_in_baselink if cropping orig.merged cloud


//        PointCloudType::Ptr pc_in_corner_frame = getTransformedObstacleCloud(intersection_point, pc_in_baselink);
//
//		std::cout<< "intersection_point : "<< intersection_point << std::endl;
//
//		sensor_msgs::PointCloud2 pc_in_corner_frame_cloud;
//		pcl::toROSMsg(*pc_in_corner_frame, pc_in_corner_frame_cloud);
//		pc_in_corner_frame_cloud.header.frame_id = "corner_frame";
//		pc_in_corner_frame_cloud.header.stamp = ros::Time::now();
//		pub_pc_cornerframe_.publish(pc_in_corner_frame_cloud);




// for recording merged pointcloud from /rgbd/depth/points_n
//		*merged_pointcloud += *pc_in_baselink;
//		i++;
//
//		if (i == 50){
//			writer.write<pcl::PointXYZ> ("/home/d/Desktop/merged_pointcloud.pcd", *merged_pointcloud, false);
//			return;
//		}





	}


}
