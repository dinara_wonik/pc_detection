#include "pc_detection/detector_node.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>



namespace pc_detection {

	DetectorNode::DetectorNode(const ros::NodeHandle &nh,
		const ros::NodeHandle &pnh)
	: nh_(nh),
	sub_pc_(nh_.subscribe("/rgbd/depth/points", 1, &DetectorNode::PointCloudCb, this)),  //point_n is voxel grid
	pub_pc_(nh_.advertise<sensor_msgs::PointCloud2>("output", 1)),
	tfListener(tf_)
	{}


	void DetectorNode::PointCloudCb(const sensor_msgs::PointCloud2ConstPtr &pc2_input_) {



		sensor_msgs::PointCloud2 pc2_input__ = *pc2_input_;
		std::cout<< "pc2_input__.header.frame_id : "<< pc2_input__.header.frame_id << std::endl;
		std::cout<< "pc2_input__.header.stamp : "<< pc2_input__.header.stamp << std::endl << std::endl;
		// pub_pc_.publish(pc2_input__);



		PointCloudType::Ptr pc_in = PointCloudType::Ptr(new PointCloudType());
		fromROSMsg(*pc2_input_, *pc_in);

		// down sampling
		PointCloudType::Ptr pc_v = PointCloudType::Ptr(new PointCloudType());
		pcl::VoxelGrid<pcl::PointXYZ> vg;

		{
			vg.setLeafSize(0.01, 0.01, 0.01);
			vg.setFilterFieldName("z");
			vg.setFilterLimits(0, RANGE_MAX);
			vg.setInputCloud(pc_in);
			vg.filter(*pc_v);
		}


		std::cout<< "pc_v header.frame_id : "<< pc_v->header.frame_id.c_str() << std::endl;
		std::cout<< "pc_v header.stamp : "<< pc_v->header.stamp << std::endl << std::endl;

		std::string output_frame_id = "base_link";
        // transform
        PointCloudType::Ptr pc_t = PointCloudType::Ptr(new PointCloudType());
        if(!pcl_ros::transformPointCloud(output_frame_id, *pc_v, *pc_t, tf_)) {
            ROS_ERROR("transform error. %s->%s", pc_v->header.frame_id.c_str(), output_frame_id.c_str());
            return;
        } else {
        	ROS_INFO("transform ok");
        }

        // pcl_ros::transformPointCloud(*pc_v, *pc_t, transform);




		// Publish the data
		sensor_msgs::PointCloud2 output;
		toROSMsg(*pc_v, output);
		pub_pc_.publish(output);

	}


}


// https://github.com/behnamasadi/cropping_pointcloud/blob/master/src/src/cropping_pointcloud.cpp
//		PointCloudType::Ptr no_plane_cloud = PointCloudType::Ptr(new PointCloudType());
//
//	    // Cloud indices representing planar components inliers
//	    pcl::PointIndices::Ptr planar_inliers (new pcl::PointIndices);
//	    // Cloud coefficients for planar components inliers
//	    pcl::ModelCoefficients::Ptr planar_coefficients (new pcl::ModelCoefficients);
//	    // Segmentation object
//	    pcl::SACSegmentation<pcl::PointXYZ> SAC_filter;
//	    pcl::ExtractIndices<pcl::PointXYZ> planar_inliers_extraction;
//
//
//	    // Segmentation object initialization
//	    SAC_filter.setOptimizeCoefficients (true);
//	    SAC_filter.setModelType(pcl::SACMODEL_PLANE);
//	    SAC_filter.setMethodType (pcl::SAC_RANSAC);
//	    SAC_filter.setMaxIterations (100);
//	    SAC_filter.setDistanceThreshold (0.02);
//
//	    // Segment the dominant plane cluster
//	    SAC_filter.setInputCloud (pc_in_baselink);
//	    SAC_filter.segment (*planar_inliers, *planar_coefficients);
//
//
//	    // Remove the planar cluster from the input cloud
//	    planar_inliers_extraction.setInputCloud (pc_in_baselink);
//	    planar_inliers_extraction.setIndices (planar_inliers);
//	    planar_inliers_extraction.setNegative (true);
//	    planar_inliers_extraction.filter (*no_plane_cloud);
//	    std::vector<int> no_Nan_vector;
//	    pcl::removeNaNFromPointCloud(*no_plane_cloud,*no_plane_cloud,no_Nan_vector);




// positive/negative obstacle
//		int positive_obstacle_upper_limit = 1.5;
//		int positive_obstacle_lower_limit = 0.0;
//		int negative_obstacle_upper_limit = -0.2;
//		double orf_search_radius = 0.1; // meter
//		int orf_neighbors_min = 3;
//
//        pcl::PassThrough<pcl::PointXYZ> pass_through;
//		PointCloudType::Ptr pc_pos = PointCloudType::Ptr(new PointCloudType());
//		PointCloudType::Ptr pc_neg = PointCloudType::Ptr(new PointCloudType());
//
//        // positive obstacle
//        PointCloudType::Ptr pc_p_tmp = PointCloudType::Ptr(new PointCloudType());
//        {
//            pass_through.setFilterFieldName("z");
//            pass_through.setFilterLimits(positive_obstacle_lower_limit, positive_obstacle_upper_limit);
//            pass_through.setNegative(true);
//            pass_through.setInputCloud(pc_in_baselink);
//            pass_through.filter(*pc_p_tmp);
//        }
//
//
//       pcl::RadiusOutlierRemoval<pcl::PointXYZ> rm_outlier;
//       {
//           rm_outlier.setRadiusSearch(orf_search_radius);
//           rm_outlier.setMinNeighborsInRadius(orf_neighbors_min);
//           rm_outlier.setNegative(true);
//       }
//
//       if(pc_p_tmp->size() > 0) {
//           rm_outlier.setInputCloud(pc_p_tmp);
//           rm_outlier.filter(*pc_pos);
//       }




