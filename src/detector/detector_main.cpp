#include "pc_detection/detector_node.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "pc_detector");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  try {
    pc_detection::DetectorNode detector_node(nh, pnh);

    printf("------------in detector_main.cpp------------\n");

//    detector_node.getICP();
    ros::spin();
  }
  catch (const std::exception &e) {
    ROS_ERROR("%s: %s", nh.getNamespace().c_str(), e.what());
  }
}
