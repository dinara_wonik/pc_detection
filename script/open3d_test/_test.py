#!/usr/bin/python3
# -*- coding: utf8 -*-#


# http://www.open3d.org/docs/release/index.html 


import os 
import re
import glob
import copy
import logging 
from functools import partial
import open3d as o3d
import numpy as np
import time


def test1():
    pcd_file = "/media/d/SSD_T5/06.24/obstacle/original_merged_1.pcd"
    pcd = o3d.io.read_point_cloud(pcd_file)
    o3d.visualization.draw_geometries([pcd,])


def test2():
    pcd_file = "./data/pointclouds_for_icp_0624_1/pc_110.pcd"
    pcd = o3d.io.read_point_cloud(pcd_file)
    o3d.visualization.draw_geometries([pcd,])


def test3():
    pcd1 = o3d.io.read_point_cloud("./data/pointclouds_for_icp_0624_1/pc_10.pcd")
    pcd2 = o3d.io.read_point_cloud("./data/pointclouds_for_icp_0624_1/pc_110.pcd")
    o3d.visualization.draw_geometries([pcd1, pcd2, ])


def rename_files(path):
    fnames = glob.glob('%s/*.pcd'%(path)) 
    p = re.compile("pc_.*.pcd")
    for src in fnames:
        a = re.findall(p, src)[0]
        number = int(re.findall('\d+', a)[0])
        dir_name = os.path.dirname(src)
        new_file_name = os.path.join(dir_name, "pc_%04d.pcd"%(number))
        print("rename %s -> %s"%(src, new_file_name))
        os.rename(src, new_file_name)


def read_file_list(path):
    fnames = glob.glob('%s/*.pcd'%(path)) 
    fnames.sort()
    return fnames



def draw_registration_result(source, target, transformation=np.identity(4)):
    source_temp = o3d.geometry.PointCloud(source)
    target_temp = o3d.geometry.PointCloud(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])  


def icp_test_1():
    files = read_file_list("./data/pointclouds_for_icp_0624_2")

    print("## number of files : %d"%(len(files)))


    source = o3d.io.read_point_cloud(files[0])
    target = o3d.io.read_point_cloud(files[50])
    threshold = 0.02

    trans_init = np.array([[1., 0., 0., 0],
                       [0., 1., 0., 0],
                       [0., 0., 1., 0],
                       [0., 0., 0., 1.]])

    print("Apply point-to-point ICP")
    reg_p2p = o3d.pipelines.registration.registration_icp(
        source, target, threshold, trans_init,
        o3d.pipelines.registration.TransformationEstimationPointToPoint())



    # reg_p2p = registration_p_to_plane(source, target, trans_init)
    print(reg_p2p)
    print("Transformation is:")
    print(reg_p2p.transformation)
    draw_registration_result(source, target, reg_p2p.transformation)

    return


def icp_test_2():
    files = read_file_list("/media/d/SSD_T5/06.24/data/pointclouds_for_icp_0624_2")

    # files = files[0:-1:5]

    for f in files:
        print(f)

    threshold = 0.05
    trans_init = np.array([[1., 0., 0., 0],
                           [0., 1., 0., 0],
                           [0., 0., 1., 0],
                           [0., 0., 0., 1.]])

    target = o3d.io.read_point_cloud(files[0])
    for f in files[1:]:
        source = o3d.io.read_point_cloud(f)

        reg_p2p = o3d.pipelines.registration.registration_icp(
            source, target, threshold, trans_init,
            o3d.pipelines.registration.TransformationEstimationPointToPoint())

        target += source.transform(reg_p2p.transformation)
        target = target.voxel_down_sample(voxel_size=0.02) 

    target = target.voxel_down_sample(voxel_size=0.02) 
    o3d.visualization.draw_geometries([target,]) 


def downsample():
    c = o3d.io.read_point_cloud("/media/d/SSD_T5/06.24/obstacle/global_cloud_2.pcd")
    c = c.voxel_down_sample(voxel_size=0.06) 
    o3d.io.write_point_cloud("/media/d/SSD_T5/06.24/obstacle/global_cloud_2_voxel_006.pcd", c)


def check_planars():

    pcd = o3d.io.read_point_cloud("/home/d/Desktop/chair_cluster.pcd")
    # pcd = pcd.voxel_down_sample(voxel_size=0.06) 
    plane_model, inliers = pcd.segment_plane(distance_threshold=0.01,
                                         ransac_n=3,
                                         num_iterations=1000)
    [a, b, c, d] = plane_model
    print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

    inlier_cloud = pcd.select_by_index(inliers)
    inlier_cloud.paint_uniform_color([1.0, 0, 0])
    outlier_cloud = pcd.select_by_index(inliers, invert=True)
    o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud],
                                      zoom=0.8,
                                      front=[-0.4999, -0.1659, -0.8499],
                                      lookat=[2.1813, 2.0619, 2.0999],
                                      up=[0.1204, -0.9852, 0.1215])
if __name__ == "__main__":
    # icp_test_2()
    # rename_files("/media/d/SSD_T5/06.24/data/pointclouds_for_icp_0624_2")
    # downsample()
    check_planars()




