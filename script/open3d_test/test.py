#!/usr/bin/python3
# -*- coding: utf8 -*-#


# http://www.open3d.org/docs/release/index.html 


import os 
import re
import glob
import copy
import logging 
from functools import partial
import open3d as o3d
import numpy as np
import time


def test1():
    pcd_file = "/media/d/SSD_T5/06.24/obstacle/original_merged_1.pcd"
    pcd = o3d.io.read_point_cloud(pcd_file)
    o3d.visualization.draw_geometries([pcd,])


def test2():
    pcd_file = "./data/pointclouds_for_icp_0624_1/pc_110.pcd"
    pcd = o3d.io.read_point_cloud(pcd_file)
    o3d.visualization.draw_geometries([pcd,])


def test3():
    pcd1 = o3d.io.read_point_cloud("./data/pointclouds_for_icp_0624_1/pc_10.pcd")
    pcd2 = o3d.io.read_point_cloud("./data/pointclouds_for_icp_0624_1/pc_110.pcd")
    o3d.visualization.draw_geometries([pcd1, pcd2, ])


def draw_registration_result(source, target, transformation=np.identity(4)):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])  


def draw_pcs(pcs, transformation_list, colors):
    pcs_tmp = []
    for pc, tr, color in zip(pcs, transformation_list, colors):
        tmp = copy.deepcopy(pc)
        tmp.paint_uniform_color(color)
        tmp.transform(tr)
        pcs_tmp.append(tmp)
    o3d.visualization.draw_geometries(pcs_tmp) 


def get_pc_files():
    file_paths = []
    file_nums = []
    file_paths_sorted = []
    for infile in glob.glob('/media/d/SSD_T5/06.24/obstacle/pointclouds_for_icp_0624_1/*.pcd'):
        # print("Current File Being Processed is: " + infile)
        file_paths.append(infile)
        current_file_name = os.path.basename(infile)
        # print(current_file_name)
        current_file_name = current_file_name[3: len(current_file_name)-4]
        # print(current_file_name)

        file_nums.append(int(current_file_name))

    # file_nums = sorted(file_nums)
    # print(file_nums)
    file_idxs = sorted(range(len(file_nums)), key = lambda k: file_nums[k])
    # print(file_idxs)

    for idx in range(0, len(file_idxs)):
        file_paths_sorted.append(file_paths[file_idxs[idx]])

    for p in file_paths_sorted:
        print(p)
    return file_paths_sorted


def registration_p_to_plane(source, target, trans_init, threshold=0.5):      
    search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30)
    
    source=copy.deepcopy(source)  
    source.estimate_normals(search_param=search_param)

    target=copy.deepcopy(target)
    target.estimate_normals(search_param=search_param)

    reg = o3d.pipelines.registration.registration_icp(
        source, target, threshold, trans_init,
        o3d.pipelines.registration.TransformationEstimationPointToPlane())
            
    return reg 


def get_icp_2pcs(file1, file2):

    files = get_pc_files()


    print(files[0])
    print(files[1])
    source = file1
    target = file2
    threshold = 0.02

    trans_init = np.array([[1., 0., 0., 0],
                       [0., 1., 0., 0],
                       [0., 0., 1., 0],
                       [0., 0., 0., 1.]])

    print("Apply point-to-point ICP")
    reg_p2p = o3d.pipelines.registration.registration_icp(
        source, target, threshold, trans_init,
        o3d.pipelines.registration.TransformationEstimationPointToPoint())

    # reg_p2p = registration_p_to_plane(source, target, trans_init)
    print(reg_p2p)
    print("Transformation is:")
    print(reg_p2p.transformation)

    global_cloud = source + target
    
    # add = o3d.io.read_point_cloud('/media/d/SSD_T5/06.24/obstacle/pointclouds_for_icp_0624_1/pc_185.pcd')
    # global_cloud+= add

    global_cloud.paint_uniform_color([1, 0.706, 0])
    o3d.visualization.draw_geometries([global_cloud])  

    # draw_registration_result(source, target, reg_p2p.transformation)

    return



def get_icp():

    files = get_pc_files()

    threshold = 0.01
    #default trans_init
    trans_init = np.array([[1., 0., 0., 0],
                       [0., 1., 0., 0],
                       [0., 0., 1., 0],
                       [0., 0., 0., 1.]])

    global_cloud = o3d.io.read_point_cloud(files[0])
    target = o3d.io.read_point_cloud(files[0])
    for idx in range(0, len(files)):
    #     global_cloud+=o3d.io.read_point_cloud(files[idx])
    #     global_cloud.paint_uniform_color([1, 0.706, 0])
    # o3d.visualization.draw_geometries([global_cloud])  



        source = o3d.io.read_point_cloud(files[idx])
        
        # print("Apply point-to-point ICP")
        reg_p2p = o3d.pipelines.registration.registration_icp(
            source, target, threshold, trans_init,
            o3d.pipelines.registration.TransformationEstimationPointToPoint(),
            o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration=2000))

        # print("Apply point-to-plane ICP")
        # reg_p2p = registration_p_to_plane(source, target, trans_init, threshold)



        global_cloud += source
        target = global_cloud
        # o3d.io.write_point_cloud("/media/d/SSD_T5/06.24/obstacle/merged1_icp/"+ str(idx)+".pcd", target)

    orig_source = o3d.io.read_point_cloud("/media/d/SSD_T5/06.24/obstacle/original_merged_1.pcd")
    draw_registration_result(orig_source, target, reg_p2p.transformation)
    print(reg_p2p)
    print("Transformation is:")
    print(reg_p2p.transformation)



    return


def read_merged_icp():
    pcs_tmp = o3d.io.read_point_cloud('/media/d/SSD_T5/06.24/obstacle/merged1_icp/110.pcd')
    pcs_tmp.paint_uniform_color([1, 0.706, 0])

    o3d.visualization.draw_geometries([pcs_tmp])


if __name__ == "__main__":


    # read_merged_icp()

    get_icp()
    # pcs_tmp = o3d.io.read_point_cloud('/media/d/SSD_T5/06.24/obstacle/pointclouds_for_icp_0624_1/pc_190.pcd')
    # pcs_tmp2 = o3d.io.read_point_cloud('/media/d/SSD_T5/06.24/obstacle/pointclouds_for_icp_0624_1/pc_210.pcd')

    # get_icp_2pcs(pcs_tmp, pcs_tmp2)


    # pcs_tmp.paint_uniform_color([1, 0.706, 0])
    # pcs_tmp2.paint_uniform_color([0, 0.651, 0.929])

    # o3d.visualization.draw_geometries([pcs_tmp, pcs_tmp2])