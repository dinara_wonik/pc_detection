#!/usr/bin/env python2
# coding: utf-8
# https://pcl.gitbook.io/tutorial/part-1/part01-chapter01/part01-chapter01-practice

import rospy
import numpy as np
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud

import pcl
import pcl_helper


i = 0

def callback(ros_cloud):

	print(ros_cloud.points)
	cloud = pcl_helper.ros_to_pcl_xyz(ros_cloud)

	print(cloud)
	cloud_new = pcl_helper.pcl_to_ros_xyz(cloud)
	pub.publish(cloud_new)





def callback_pointcloud(data):
	global cloud_points
	print("in callback -----------------------------")

	xyz = np.array([[0,0,0]])
	# assert isinstance(data, PointCloud2)
	gen = pc2.read_points(data)
	pointcloud_data_list = list(gen)
	print("pointcloud array: ")
	for x in pointcloud_data_list:
		xyz = np.append(xyz,[[x[0],x[1],x[2]]], axis = 0)
		cloud_points = np.append(cloud_points,[[x[0],x[1],x[2]]], axis = 0)

	xyz = xyz[1:]
	print(xyz)
	print(xyz.shape)


# if __name__ == "__main__":

# 	cloud_points = np.array([[0,0,0]])

# 	rospy.init_node('sub_pc_node', anonymous=True)
# 	rospy.Subscriber('/rgbd/depth/points_n', PointCloud2, callback_pointcloud)


# 	# pub = rospy.Publisher("/merged_pointcloud", PointCloud2, queue_size=1, latch = True)
# 	rospy.spin()


# 	cloud_points = cloud_points[1:]
# 	print(cloud_points)
# 	print(cloud_points.shape)

# 	x_axis_vals = cloud_points[:, 0]
# 	print(x_axis_vals.shape)
# 	print(x_axis_vals[0])

# 	y_axis_vals = cloud_points[:, 1]
# 	print(y_axis_vals.shape)
# 	print(y_axis_vals[0])

# 	z_axis_vals = cloud_points[:, 2]
# 	print(z_axis_vals.shape)
# 	print(z_axis_vals[0])


# 	print("min x axis: ", np.min(x_axis_vals))
# 	print("min y axis: ", np.min(y_axis_vals))
# 	print("min z axis: ", np.min(z_axis_vals))

# 	print("max x axis: ", np.max(x_axis_vals))
# 	print("max y axis: ", np.max(y_axis_vals))
# 	print("max z axis: ", np.max(z_axis_vals))




# .pcd publishing node
def talker():
	# pub = rospy.Publisher("/merged_pointcloud", PointCloud2, queue_size=1, latch = True)
	pub2 = rospy.Publisher("/icp_aligned_cloud", PointCloud2, queue_size=1, latch = True)


	# p_orig = pcl.load("/media/d/SSD_T5/06.24/obstacle/original_merged_1.pcd") 
	# p = pcl.load("/media/d/SSD_T5/06.24/obstacle/global_cloud_2.pcd")  #pcl::PointXYZ poi_point(2.15, 0.02, 0.0); error case #and other poi point also
	# may be because of incorrectly defined planes
	
	# p = pcl.load("/media/d/SSD_T5/06.24/obstacle/global_cloud_2.pcd")
	# p = pcl.load("/media/d/SSD_T5/06.16/outer_corner_merged_pointcloud_no_obstacle.pcd") #outer_corner_merged_pointcloud_no_obstacle

	p = pcl.load("/media/d/SSD_T5/06.24/obstacle/global_cloud_2_voxel_006.pcd")
	# p = pcl.load("/home/d/Desktop/global_cloud_2_nonfloor.pcd")


	# p = pcl.load("/home/d/Desktop/samp11-utm_object.pcd")

	# print("original pointcloud size: ", p_orig.size)
	# print("icp aligned pointcloud size: ", p.size)

	# p_new = pcl_helper.pcl_to_ros_xyz(p_orig)
	p_new2 = pcl_helper.pcl_to_ros_xyz(p)

	rospy.init_node('talker', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		# pub.publish(p_new)	
		pub2.publish(p_new2)	
		rate.sleep()

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass

