#ifndef PC_DETECTOR_NODE_H_
#define PC_DETECTOR_NODE_H_

#include <ros/ros.h>
#include <math.h> //for PI
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_geometry/pinhole_camera_model.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/crop_box.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <roscpp_tutorials/TwoInts.h>
#include <pc_detection/GetObstacle.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/sac_model.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <pcl/common/intersections.h>

#include <visualization_msgs/Marker.h>
#include <rviz/helpers/color.h>

#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
//#include <Eigen/Geometry>

//for pointToPlaneDistanceSigned
#include <pcl/common/distances.h>
#include <pcl/sample_consensus/sac_model_plane.h>

#include "boost/filesystem.hpp"
#include <pcl/registration/icp.h>
#include <pcl/registration/ia_ransac.h>


#include <pcl/segmentation/min_cut_segmentation.h>
#include <pcl/io/pcd_io.h>

#include <pcl/visualization/cloud_viewer.h>


#include <pcl/segmentation/progressive_morphological_filter.h>



using namespace boost::filesystem;


namespace pc_detection {



class DetectorNode {
	public:
		DetectorNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh);
        std::vector<path> getPointcloudFiles();
		void getICP();

	private:
	// void ConnectCb();
	// void CameraCb(const sensor_msgs::ImageConstPtr &image_msg, const sensor_msgs::CameraInfoConstPtr &cinfo_msg);
		void PointCloudCb(const sensor_msgs::PointCloud2ConstPtr &pc2_input_);

		std::string _name = ros::this_node::getName();

		ros::NodeHandle nh_;
		ros::Subscriber sub_pc_;
		ros::Publisher pub_pc_wall1_;
		ros::Publisher pub_pc_wall2_;
		ros::Publisher pub_pc_floor_;
		ros::Publisher pub_pc_obstacle_;
		ros::Publisher pub_inliers_;
		ros::Publisher pub_pc_cornerframe_;
		ros::Publisher pub_plane_cluster_;
		ros::Publisher pub_icp_aligned_;
		ros::Publisher pub_intersection_point_marker_;
		ros::Publisher pub_poi_;

		ros::ServiceServer service_;

		typedef pcl::PointCloud<pcl::PointXYZ> PointCloudType;
		typedef pcl::PCLPointCloud2            PointCloud2Type;
		sensor_msgs::PointCloud2 pc2_input__;

	// bool cam_calibrated_;
	// image_transport::ImageTransport it_;
	// image_transport::CameraSubscriber sub_camera_;


		image_geometry::PinholeCameraModel model_;
		const float RANGE_MAX = 1e9;
	// ProjectionFilter projection_filter_;

		tf2_ros::TransformBroadcaster tf_broadcaster_;
		tf2_ros::Buffer tf_;
		tf2_ros::TransformListener tfListener;

        pcl::PointCloud<pcl::PointXYZ> cloud_segmented_wall1;
        pcl::PointCloud<pcl::PointXYZ> cloud_segmented_wall2;
        pcl::PointCloud<pcl::PointXYZ> cloud_segmented_floor;
        pcl::PointCloud<pcl::PointXYZ> cloud_segmented_obstacle;


        pcl::ModelCoefficients::Ptr coefficients_floor = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);

        pcl::ModelCoefficients::Ptr coefficients_wall1 = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);
        pcl::ModelCoefficients::Ptr coefficients_wall2 = pcl::ModelCoefficients::Ptr(new pcl::ModelCoefficients);


		PointCloudType::Ptr segm = PointCloudType::Ptr(new PointCloudType());
		PointCloudType::Ptr cloud_f = PointCloudType::Ptr(new PointCloudType());
		PointCloudType::Ptr no_planars_cloud = PointCloudType::Ptr(new PointCloudType());



        Eigen::Vector4d plane_wall1, plane_wall2, plane_floor;
        Eigen::Vector3d intersection_point;
        pcl::PointIndices::Ptr inliers_wall1 = pcl::PointIndices::Ptr(new pcl::PointIndices);
        pcl::PointIndices::Ptr inliers_wall2 = pcl::PointIndices::Ptr(new pcl::PointIndices);


        Eigen::Isometry3d corner_to_baselink_tf;
        pcl::Normal norm1;
//		std::vector<pcl::ModelCoefficients> plane_coeff_list_except_floorplane;
//		std::vector<pcl::PointCloud<pcl::PointXYZ>> planes_list_except_floorplane;


        PointCloudType::Ptr setPassthroughFilter(const PointCloudType::Ptr pc_in_baselink);
        PointCloudType::Ptr getFloorPlanePerpendicularZAxis(const PointCloudType::Ptr pc_in_baselink);
        void getWalls(const PointCloudType::Ptr pc_in_baselink);
        bool checkPlanes(const PointCloudType::Ptr pc_in_baselink);

        PointCloudType::Ptr removePlanars(const PointCloudType::Ptr pc_in_baselink);
        void clusterExtraction(const PointCloudType::Ptr pc_in_baselink);


		void getIntersectionPoint(const PointCloudType::Ptr pc_in_baselink);
		void getObstacleCropbox(const PointCloudType::Ptr pc_in_baselink);
		PointCloudType::Ptr getTransformedObstacleCloud (	const Eigen::Vector3d intersection_point,
											const PointCloudType::Ptr obstacle_in_baselink);

		//misc.testing
		void mincutSegmentation(const PointCloudType::Ptr pc_in_baselink);
		void groundFiltering(const PointCloudType::Ptr cloud);

		pcl::PCDWriter writer;
		PointCloudType::Ptr merged_pointcloud = PointCloudType::Ptr(new PointCloudType());
		int i = 0;




};



}
#endif
